module bananaCap(){
    translate([0,0,3]) 
    cylinder(10,d=1.7,$fn=500,center=true);
    difference(){
        union(){
        cylinder(5,d=10,center=true,$fn=500);
        }
        translate([0,0,2]) 
        cylinder(2.6,d=8,center=true,$fn=500);
    }
}

//for (i=[0:4]){
//    for(j=[0:4]){
//    translate([i*10,j*10,2.5]) bananaCap();
//    }
//}
bananaCap();